<?php



use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';

$app = new \Slim\App([
    'settings' => [
        'displayErrorDetails' => true
    ]
]);
$container = $app->getContainer();

$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig('../templates');

    $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($container['router'], $basePath));

    return $view;
};

$app->get('/', function (Request $request, Response $response) use ($container) {

    $ic = new \App\Controller\IndexController();
    $ic->setContainer($container);
    $ic->setView($this->view);
    return $ic->getIndexResponse($request, $response);

})->setName('index');

//sending mail
$app->post('/testmail', function (Request $request , Response $response) use ($container) {

    $dotenv = new Dotenv\Dotenv("../");
    $dotenv->load();

    $ic = new \App\Controller\IndexController();
    $ic->setContainer($container);
    $ic->setView($this->view);
    return $ic->getMailResponse($request, $response);


})->setName('testmail');


$app->run();

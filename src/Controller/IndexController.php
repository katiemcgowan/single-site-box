<?php
/**
 * Created by PhpStorm.
 * User: katiemcgowan
 * Date: 24/02/2017
 * Time: 13:42
 */

namespace App\Controller;

use PHPMailer;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;


class IndexController
{
    private $container = null;
    private $view = null;

    public function setView($view) {
        $this->view = $view;
    }

    public function setContainer($container) {
        $this->container = $container;
    }


    public function getIndexResponse(Request $request, Response $response)
    {

        $done = $request->getParam('done');
        if ($done == 1){
            echo "Thank you for your message";
        } else if ($done == 2){
            echo "One or more of the fields had an error. Please correct this and submit the form again.";
        }
        return $this->view->render($response, 'index.html', [
            'name' => ', it\'s me'

        ]);
    }

    public function getMailResponse(Request $request, Response $response)
    {


        $firstname = $request->getParam('firstname');
        $lastname = $request->getParam('lastname');
        $email = $request->getParam('email');
        $phone = $request->getParam('telephone');
        $message = $request->getParam('message');


        $html = file_get_contents('../templates/email.html');
        $html = str_replace('%firstname%', $firstname, $html);
        $html = str_replace('%lastname%', $lastname, $html);
        $html = str_replace('%email%', $email, $html);
        $html = str_replace('%telephone%', $phone, $html);
        $html = str_replace('%message%', $message, $html);

        $form_err = FALSE;

        if (empty ($firstname)):

            $form_err = TRUE;

        endif;

        if (empty ($lastname)):

            $form_err = TRUE;

        endif;

        if (empty ($email) || filter_var($email, FILTER_VALIDATE_EMAIL) != $email):

            $form_err = TRUE;

        endif;

        if (empty ($phone)):

            $form_err = TRUE;

        endif;

        if (empty ($message)):

            $form_err = TRUE;

        endif;

        if ($form_err != TRUE) {

            $mail = new PHPMailer;


            $mail->SMTPDebug = getenv('SMTP_DEBUG');  // Enable verbose debug output
//            $mail->isSMTP();   // Set mailer to use SMTP
            $mail->Host = getenv('HOST');  // Specify main and backup SMTP servers
            $mail->SMTPAuth = getenv('SMTP_AUTH');  // Enable SMTP authentication
            $mail->Username = getenv('USERNAME'); // SMTP username
            $mail->Password = getenv('PASSWORD'); // SMTP password
            //     $mail->SMTPSecure = ''; // Enable TLS encryption, `ssl` also accepted
            $mail->Port = getenv('PORT');  // TCP port to connect to

            $mail->setFrom(getenv('FROM'));
            $mail->addAddress(getenv('TO'));

            $mail->isHTML(true); // Set email format to HTML

            $mail->Subject = 'Message from ' . $firstname . ' ' . $lastname;
            $mail->msgHTML($html);

            if (!$mail->send()) {
                echo 'Message could not be sent.';
                echo 'Mailer Error: ' . $mail->ErrorInfo;
            } else {
                echo 'Message has been sent';
                header('Location: /?done=1');

            }
        } else {
            header('Location: /?done=2');
        }

    }
}